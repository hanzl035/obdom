// creating loader for Fragment class
const opener = new RegExp(
 "<(?!!--)([^]+?)(?<!--)>|<!--([^]+?)-->|([^<>]+)",
 "g"
)
const comment_cleaner = new RegExp("<!--([^]+?)-->", "g")
const selfclosing_cleaner = new RegExp("/$")
const closing_tester = new RegExp("^/")
const namer_cleaner = new RegExp("/?\\s*([^\\s]+)")
const attributer = new RegExp('([^\\s=]+)(?:=(?:"|\')([^\\s]+)(?:"|\'))?', "g")

// groups: 0 match, 1 tag, 2 comment, 3 textnode
function parse(match) {
 const parsed = {}
 if (typeof match[2] != "undefined") {
  parsed.found = "comment"
  parsed.content = match[2]
 }
 else if (typeof match[3] != "undefined") {
  parsed.found = "textnode"
  parsed.content = match[3].trim()
 }
 else if (typeof match[1] != "undefined") {
  parsed.comments = [...match[1].matchAll(comment_cleaner)].map(match => {
   return new Comment(match[1])
  })
  const commentless = match[1].replace(comment_cleaner, "")
  if (closing_tester.test(commentless)) {
   parsed.found = "closetag"
   parsed.name = namer_cleaner.exec(match[1])[1]
  }
  else {
   if (selfclosing_cleaner.test(commentless)) parsed.found = "emptyelement"
   else parsed.found = "element"
   const closeless = commentless.replace(selfclosing_cleaner, "")
   parsed.name = namer_cleaner.exec(closeless)[1]
   const nameless = closeless.replace(namer_cleaner, "")
   parsed.attributes = new Attributes()
   for (let match of nameless.matchAll(attributer)) {
    parsed.attributes.set(match[1], match[2] || true)
   }
  }
 }
 return parsed
}


const prepper = new RegExp(
 "^<!DOCTYPE html>\\s+|^\\s+|(?<=>)\\s+(?=<)|\\s+$",
 "g"
)

class Attributes extends Map {
 constructor(iterable) {
  super(iterable)
 }
 toString() {
  let text = ""
  for (let entry of this.entries()) {
   if (typeof entry[1] == "boolean") {
    if (entry[1]) text = text + " " + entry[0]
   }
   else if (typeof entry[1] == "string") {
    text = text + " " + entry[0] + "=\"" + entry[1] + "\""
   }
  }
  return text
 }
}

class EmptyElement {
 constructor(tag_name, attributes) {
  this.tag_name = tag_name
  this.attributes = new Attributes(attributes)
 }
 clone() {
  return new EmptyElement(this.tag_name, this.attributes)
 }
 toString() {
  return "<" + this.tag_name + this.attributes + ">"
 }
 toPretty(spaces = 0, limit = 60) {
  return " ".repeat(spaces) + this.toString()
 }
}

class Element extends Array {
 constructor(tag_name, attributes, ...nodes) {
  super(...nodes)
  this.tag_name = tag_name
  this.attributes = new Attributes(attributes)
 }
 clone() {
  return new Element(
   this.tag_name,
   this.attributes,
   ...this.map(node => node.clone())
  )
 }
 toString() {
  return this.reduce((text, node) => {
   return text + node
  }, "<" + this.tag_name + this.attributes + ">") + "</" + this.tag_name + ">"
 }
 toPretty(spaces = 0, limit = 60) {
  return this.reduce(
   (text, node) => {
    if (typeof node.toPretty == "function") {
     return text + node.toPretty(spaces + 1, limit + 1) + "\n"
    }
    else {
     return text + node.toString() + "\n"
    }
   },
   " ".repeat(spaces) + "<" + this.tag_name + this.attributes + ">" + "\n"
  ) + " ".repeat(spaces) + "</" + this.tag_name + ">"
 }
 querySelector = querySelector.bind(this)
}

class Fragment extends Array {
 static load(text) {
  const prepped = text.replace(prepper, "")
  const fragment = new Fragment()
  const history = []
  let target = fragment
  for (let match of prepped.matchAll(opener)) {
   const parsed = parse(match)
   // for simplicity, we pop comments inside tags into the elements
   if (parsed.found == "comment") {
    target.push(new Comment(parsed.content))
   }
   else if (parsed.found == "textnode") {
    target.push(new TextNode(parsed.content))
   }
   else if (parsed.found == "emptyelement") {
    target.push(...parsed.comments)
    target.push(new EmptyElement(parsed.name, parsed.attributes))
   }
   else if (parsed.found == "element") {
    target.push(...parsed.comments)
    history.push(target)
    target = new Element(parsed.name, parsed.attributes)
    history[history.length - 1].push(target)
   }
   else if (parsed.found == "closetag") {
    target.push(...parsed.comments)
    if (target.tag_name != parsed.name) {
     throw new Error("dummy check failed")
    }
    target = history.pop()
   }
  }
  return fragment
 }

 constructor(...nodes) {
  super(...nodes)
 }
 clone() {
  return new Fragment(...this.map(node => node.clone()))
 }
 toString() {
  return this.reduce((text, node) => {
   return text + node
  }, "")
 }
 toPretty(spaces = 0, limit = 60) {
  return this.map(node => {
   if (typeof node.toPretty == "function") {
    return node.toPretty(spaces, limit)
   }
   else return node.toString()
  }).join("\n")
 }
 querySelector = querySelector.bind(this)
}

class TextNode {
 constructor(text = "") {
  this.content = text
 }
 clone() {
  return new TextNode(this.content)
 }
 toString() {
  return this.content
 }
 toPretty(spaces = 0, limit = 60) {
  const single_whitespace = new RegExp("(?<! +) (?! +)")
  const splitted = this.content.split(single_whitespace)
  let text_array = []
  let buffer = " ".repeat(spaces) + splitted[0]
  for (let i = 1; i < splitted.length; i++) {
   if (buffer.length > limit) {
    text_array.push(buffer)
    buffer = " ".repeat(spaces) + splitted[i]
   }
   else {
    buffer = buffer + " " + splitted[i]
   }
  }
  text_array.push(buffer)
  return text_array.join("\n")
 }
}

class Comment {
 constructor(text = "") {
  this.content = text
 }
 get closed_content() {
  return "<!--" + this.content + "-->"
 }
 clone() {
  return new Comment(this.content)
 }
 toString() {
  return this.closed_content
 }
 toPretty(spaces = 0, limit = 60) {
  const single_whitespace = new RegExp("(?<! +) (?! +)")
  const splitted = this.closed_content.split(single_whitespace)
  let text_array = []
  let buffer = " ".repeat(spaces) + splitted[0]
  for (let i = 1; i < splitted.length; i++) {
   if (buffer.length > limit) {
    text_array.push(buffer)
    buffer = " ".repeat(spaces) + splitted[i]
   }
   else {
    buffer = buffer + " " + splitted[i]
   }
  }
  text_array.push(buffer)
  return text_array.join("\n")
 }
}

class Templates {
 static linklist(tag_name, attributes, iterable) {
  const list = new Element(tag_name, attributes)
  for (let entry of iterable) {
   const href = entry[1]
   const content = new TextNode(entry[0])
   const anchor = new Element("a", [["href", href]], content)
   const li = new Element("li", undefined, anchor)
   list.push(li)
  }
  return list
 }
}

// to be bound to query-able elements
// #warning: only understands singles tagname, class, id, for now
function querySelector(prompt) {
 const id_test = new RegExp("^#")
 const class_test = new RegExp("^.")
 const tag_test = new RegExp("^[A-Za-z]")
 let test
 if (id_test.test(prompt)) {
  const id = prompt.replace(id_test, "")
  test = (element) => {
   return (element.attributes && element.attributes.get("id") == id)
  }
 }
 else if (class_test.test(prompt)) {
  const class_text = prompt.replace(class_test, "")
  test = (element) => {
   if (element.attributes) {
    const classes = element.attributes.get("class").split(" ")
    return classes.some(classic => classic == class_text)
   }
   else return false
  }
 }
 else if (tag_test.test(prompt)) {
  test = (element) => {
   return (element.tag_name && element.tag_name == prompt)
  }
 }
 else throw new Error("prompt not understood")
 const future = [this]
 while (future.length != 0) {
  for (let entry of future.pop()) {
   if (test(entry)) {
    return entry
   }
   if (
    entry instanceof Element ||
    entry instanceof Fragment
   ) future.push(entry)
  }
 }
}

export {
 Attributes, Comment, Element, EmptyElement, Fragment, Templates, TextNode
}
